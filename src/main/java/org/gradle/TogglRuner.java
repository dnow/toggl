package org.gradle;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import ch.simas.jtoggl.JToggl;
import ch.simas.jtoggl.TimeEntry;

public class TogglRuner {
    
    private static final String API_TOKEN = "19d870266b1539ae4aa1e50445c3457d";
    
    //private Long projectId = 22189795L; CLAVIS
    private Long projectId = 19098838L; //  servcie gateway 
    
    //private Long taksId = 10302000L; CLAVIS 
    private Long taksId = 9816214L; // servcie gateway 
    
    private int startHour = 8;
    
    private int endHour = 16;
    
    private int duration = 60 * 60 * 8;
    
    private List<String> tags = Arrays.asList("Development");
    
    private static JToggl jToggl;
    
    public TogglRuner() {
        init();
    }
    
    private void init() {
        jToggl = new JToggl(API_TOKEN);
        jToggl.setThrottlePeriod(500l);
        jToggl.switchLoggingOn();
    }
    
    protected List<Long> createEntries(int month, String description, int ... days ) {
        List<Long> ids = new ArrayList<Long>();
        for (int d : days) {
            ids.add(createEntry(month, d, description).getId());
        }
        return ids;
    }
    
    protected List<Long> createMonthWorkingDaysEntries(int month, String description, Integer ... exceptDays) {
        Calendar cal = Calendar.getInstance();
        cal.set(2016, month, 1, startHour, 0);
        int maxDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        return createWorkingDaysEntries(month, 1, maxDay, description, exceptDays);
    }
    
    protected List<Long> createWorkingDaysEntries(int month, int from, int to, String description, Integer ... exceptDays) {
        List<Integer> exceptList = Collections.emptyList();
        if(exceptDays!= null && exceptDays.length > 0) {
            exceptList = Arrays.asList(exceptDays);
        }
        List<Long> ids = new ArrayList<Long>();
        Calendar cal = Calendar.getInstance();
        for (int d = from; d <= to; d++) {
            cal.set(2016, month, d, startHour, 0);
            int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
            if(dayOfWeek != Calendar.SATURDAY && dayOfWeek != Calendar.SUNDAY && !exceptList.contains(d)) {
                ids.add(createEntry(cal, description).getId());
            }
        }
        System.out.println(ids.toString());
        return ids;
    }
    
    protected TimeEntry createEntry(int month, int day, String description) {
        Calendar cal = Calendar.getInstance();
        cal.set(2016, month, day, startHour, 0);
        return createEntry(cal, description);
    }
    
    protected TimeEntry createEntry(Calendar cal, String description) {
        TimeEntry entry = new TimeEntry();
        entry.setDuration(duration);
        entry.setPid(projectId);
        entry.setTid(taksId);
        entry.setTag_names(tags);
        entry.setBillable(true);
        cal.set(Calendar.HOUR_OF_DAY, startHour);
        entry.setStart(cal.getTime());
        cal.set(Calendar.HOUR_OF_DAY, endHour);
        entry.setStop(cal.getTime());
        entry.setDescription(description);
        entry.setCreated_with("TogglRuner");

        return jToggl.createTimeEntry(entry);
    }
    
    public static void main(String[] args) {
        TogglRuner runer = new TogglRuner();
        
        // !!! this use java month numbering so for example January is 0 and July is 6 
        
        // will generate single entry in 15 July
        //runer.createEntry(6, 15, "Development");
        
        // will generate 9 entries for each day from 6-18 excluding Saturdays and Sundays
        //runer.createWorkingDaysEntries(6, 6, 18, "Project management");
        
        // will generate 7 entries for each day from 6-18 excluding 13 and 14 and Saturdays and Sundays
        //runer.createWorkingDaysEntries(6, 6, 18, "Project management", 13, 14);
        
        //will generate 21 entries for August excluding 15 and 25 and 26 and Saturdays and Sundays 
        //runer.createMonthWorkingDaysEntries(7, "Development", 15, 25, 26);
        
        
    }
}
